import os
import requests
from openpyxl import load_workbook

class NasdaqNordicList:
#----------------------------------------------------------------------------------------------------------------
# Members

    _receivedFile = False
    _fileUrl = 'http://www.nasdaqomxnordic.com/digitalAssets/110/110492_the-nordic-list-july-22-2020.xlsx'
    _filename = 'stockList.xlsx'
    _sheetName = 'Nordic list'
    _symbolColumnLetter = 'G'
    _exchangeColumnLetter = 'C'
    _rowStartIndex = 7

#----------------------------------------------------------------------------------------------------------------
# Pulic

    # Download NasdaqNordicList from Url
    def downloadFile(self):
        print('Retriving file from url ' + self._fileUrl + ' ... ')

        response = requests.get(self._fileUrl)
        print('Reponse: ' + str(response.status_code))

        if response.status_code == 200:
                self._receivedFile = True
                file = open(self._filename, 'wb')
                file.write(response.content)
                file.close()

    # Get all Symbols from List, can be filtered by Exchange
    def getSymbols(self, exchange):
        symbols = []
        if self._receivedFile == False:
            print('Can\'t get any symbols, no file has been downloaded')
            return symbols

        workbook = load_workbook(filename = self._filename)
        worksheet = workbook[self._sheetName]
        worksheet.delete_rows(1, self._rowStartIndex-1)

        rows = []
        companiesLength = len(worksheet[self._exchangeColumnLetter])
        print('Found ' + str(companiesLength) + ' listed companies')
        for i in range(0, companiesLength):
            rows.append(self._RowData(i))

        print('Filtering companies where exchange match ' + exchange)
        self.__filterRows(rows, worksheet[self._exchangeColumnLetter], exchange)

        symbols = self.__getSymbolsInRows(worksheet, rows)
        print('Returning ' + str(len(symbols)) + ' symbols')
        return symbols

#----------------------------------------------------------------------------------------------------------------
# Private

    class _RowData:
        def __init__(self, index):
            self._filtered = False
            self._index = index

    # Tag Row as filtered
    def __filterRows(self, rows, column, filter):
        for rowIndex in range(0, len(column)-1):
            if column[rowIndex].value != filter:
                rows[rowIndex]._filtered = True

    # Get All Symbols from row indices
    def __getSymbolsInRows(self, worksheet, rows):
        symbols = []
        for row in rows:
            if row._filtered == False:
                cellIndex = self._symbolColumnLetter + str(row._index)
                symbols.append(worksheet[cellIndex].value)
        return symbols

    # Destruct
    def __del__(self):
        if self._receivedFile == True:
            os.remove(self._filename)

