from nasdaqNordicList import NasdaqNordicList

if __name__ == '__main__':
    nasdaqList = NasdaqNordicList()
    nasdaqList.downloadFile()
    result = nasdaqList.getSymbols('STO')
    print(result)
    print('List size: ' + str(len(result)))
